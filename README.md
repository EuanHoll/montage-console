Montage Console

A C# command line program that can create simple montages from videos based upon the FFMPEG library.

Features 
- Random Selection
- Video Bounces
- Convert Files

Known Bugs
- A multi threading issue with some of the renders.