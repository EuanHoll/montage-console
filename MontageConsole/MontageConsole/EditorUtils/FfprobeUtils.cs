﻿using System;
using Shell32;
using MontageConsole.Utils;
using System.Threading;

namespace MontageConsole.EditorUtils
{
	class FfprobeUtils
	{
		private static int videoSeconds = 0;

		public static int GetVideoLength(string input)
		{
			if (Thread.CurrentThread.GetApartmentState() == ApartmentState.STA)
			{
				ShellVideoLength(input);
			}
			else
			{
				Thread staThread = new Thread(new ParameterizedThreadStart(ShellVideoLength));
				staThread.SetApartmentState(ApartmentState.STA);
				staThread.Start(input);
				staThread.Join();
			}
			return (videoSeconds);
		}

		private static void ShellVideoLength(object inp)
		{
			string input = (string)inp;
			var shell = new Shell();
			var folder = shell.NameSpace(Environment.CurrentDirectory);
			int seconds = 0;

			foreach (FolderItem2 item in folder.Items())
			{
				if (item.Name == StringUtils.GetStringMinusExtension(input))
				{
					seconds = (int)(item.ExtendedProperty("System.Media.Duration") / 10000000);
				}
			}

			videoSeconds = seconds;
		}
	}
}
