﻿using MontageConsole.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MontageConsole.EditorUtils
{
	static class FileManager
	{
		public static bool FilesExists(string[] lst)
		{
			foreach (string l in lst)
			{
				if (!File.Exists(l))
					return (false);
			}
			return (true);
		}

		public static void CreateTemp()
		{
			Directory.CreateDirectory(FinalVariables.tmpDir);
		}

		public static void DeleteTemp()
		{
			try
			{
				string[] files = Directory.GetFiles(FinalVariables.tmpDir);
				foreach (string str in files)
				{
					File.Delete(str);
				}
				Directory.Delete(FinalVariables.tmpDir);
			} catch
			{
				Console.WriteLine("Could Not Delete Files, Finishing With Errors");
			}
		}

		public static void CreateTextOrder(List<string> list)
		{ 
			List<string> lst = new List<string>();
			if (File.Exists(FinalVariables.fileOrdUniLoc))
				File.Delete(FinalVariables.fileOrdUniLoc);
			File.Create(FinalVariables.fileOrdUniLoc).Close();
			foreach (string str in list)
			{
				if (str.EndsWith(".mp4"))
				{
					lst.Add("file '" + str + "'");
				}
				else
				{
					lst.Add(File.ReadAllLines(Environment.CurrentDirectory + "\\" + str)[0]);
					lst.Add(File.ReadAllLines(Environment.CurrentDirectory + "\\" + str)[1]);
				}
			}
			lst = lst.CustomSort().ToList<string>();
			File.WriteAllLines(FinalVariables.fileOrdUniLoc, lst);
		}

		public static IEnumerable<string> CustomSort(this IEnumerable<string> list)
		{
			int maxLen = list.Select(s => s.Length).Max();

			return list.Select(s => new
			{
				OrgStr = s,
				SortStr = Regex.Replace(s, @"(\d+)|(\D+)", m => m.Value.PadLeft(maxLen, char.IsDigit(m.Value[0]) ? ' ' : '\xffff'))
			})
			.OrderBy(x => x.SortStr)
			.Select(x => x.OrgStr);
		}

		static string GetNext(List<string> list)
		{
			string next = list[0];
			if (list.Count() == 0)
				return ("NULL");
			foreach (string elm in list)
			{
				if (string.Compare(elm, next) < 0)
					next = elm;
			}
			return (next);
		}
	}
}
