﻿using Javi.FFmpeg;
using MontageConsole.Types;
using MontageConsole.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MontageConsole.EditorUtils
{
	class RandomEditor
	{

		public static void RandomEdit(string input)
		{
			List<string> files = new List<string>();
			List<Vec2> sList = new List<Vec2>();
			int length = FfprobeUtils.GetVideoLength(input);
			int begin = 0;
			Random rand = new Random();
			int size = 0;

			FileManager.CreateTemp();
			Console.WriteLine("Video Length -> " + length);
			while (begin < length)
			{
				size = rand.Next(rand.Next(2, 3), rand.Next(4, 6));
				sList.Add(new Vec2(begin, size));
				Console.WriteLine("Creating snipping area -> (Start Pos : " + begin + ", Size : " + size + ")");
				begin += size + rand.Next(2, 11);
			}
			CreateSourcesThreaded(input, files, sList);
			FinishCompiling(files, input);
		}

		public static void RandomEdit(string[] args)
		{
			List<string> files = new List<string>();
			List<Vec2> sList = new List<Vec2>();
			Random rand = new Random();
			FileManager.CreateTemp();

			foreach (string vid in args)
			{
				int length = FfprobeUtils.GetVideoLength(vid);
				int begin = 0;
				int size = 0;

				Console.WriteLine("Video Length -> " + length);
				while (begin < length)
				{
					size = rand.Next(rand.Next(2, 3), rand.Next(4, 6));
					sList.Add(new Vec2(begin, size));
					Console.WriteLine("Creating snipping area -> (Start Pos : " + begin + ", Size : " + size + ")");
					begin += size + rand.Next(2, 11);
				}
				CreateSourcesThreaded(vid, files, sList);
				sList.Clear();
			}
			FinishCompiling(files, "output");
		}

		private static void CreateSourcesThreaded(string input, List<string> files, List<Vec2> sList)
		{
			List<Thread> tList = new List<Thread>();
			Random r = new Random();
			foreach (Vec2 vec in sList)
			{
				Thread th = new Thread(() =>
				{
					if (r.Next(0, 3) == 2)
						files.Add(CreateReverse(input, vec.X, vec.Y));
					else
						files.Add(CreateCut(input, vec.X, vec.Y, 1f));
				});
				tList.Add(th);
			}
			RunThreads(tList);
		}

		private static void RunThreads(List<Thread> tList)
		{
			int i = 0;

			while (i < tList.Count())
			{
				tList[i].Start();
				if (i + 1 < tList.Count())
					tList[i + 1].Start();
				Console.WriteLine("Cutting Chunk (" + i + ")");
				tList[i].Join();
				Console.WriteLine("Cut Chunk (" + i + ")");
				if (i + 1 < tList.Count())
				{
					Console.WriteLine("Cutting Chunk (" + (i + 1) + ")");
					tList[i + 1].Join();
					Console.WriteLine("Cut Chunk (" + (i + 1) + ")");
				}
				i += 2;
			}
		}

		private static void FinishCompiling(List<string> files, string name)
		{
			Console.WriteLine("Cuts Selected, Now Compiling");
			FileManager.CreateTextOrder(files);
			try
			{
				Console.WriteLine("Finished Compiling -> " +
					CompileFinal(files, FinalVariables.fileOrdLcLoc, name));
				Console.Read();
				Console.WriteLine("Deleting TEMP Files");
				FileManager.DeleteTemp();
				Console.WriteLine("Deleted All TEMP Files");
				Console.WriteLine("Video Finished");
			}
			catch
			{
				Console.WriteLine("Compiling Failed");
				Console.WriteLine("Deleting TEMP Files");
				FileManager.DeleteTemp();
				Console.WriteLine("Deleted All TEMP Files");
				Console.WriteLine("Video Failed to Render");
			}
			Console.Read();
		}

		private static string CompileFinal(List<string> list, string input, string name)
		{
			name = StringUtils.GetFileName(input);
			name = "Output\\" + name + "_rand_edit.mp4";
			FileManager.CreateTextOrder(list);
			using (var ffmpeg = new FFmpeg(FinalVariables.ffmpegLoc))
			{
				ffmpeg.Run(FinalVariables.fileOrdLcLoc, name, FinalVariables.MergeCommand(input, name));
			}
			return (name);
		}

		private static string CreateCut(string input, int time, int size, float speed)
		{
			string name = StringUtils.GetFileName(input);
			string str = FinalVariables.tmpDir + "\\tmp_" + name + "_" + time + ".mp4";

			using (var ffmpeg = new FFmpeg(@FinalVariables.ffmpegLoc))
			{
				ffmpeg.Run(input, str, FinalVariables.CutCommand(input, str, time, size, speed));
			}
			str = "TEMP\\tmp_" + name + "_" + time + ".mp4";
			return (str);
		}

		private static string CreateReverse(string input, int time, int size)
		{
			using (var ffmpeg = new FFmpeg(@FinalVariables.ffmpegLoc))
			{
				string name = StringUtils.GetFileName(input);
				string str1 = FinalVariables.tmpDir + "\\tmp_" + name + "_" + time + "_f.mp4";
				string str2 = FinalVariables.tmpDir + "\\tmp_" + name + "_" + time + "_r.mp4";
				string loc = FinalVariables.tmpDir + "\\tmp_" + name + "_" + time + ".txt";
				List<string> tb = new List<string>();
				RCut(input, time, size, str1, ffmpeg);
				RReverse(size, str1, str2, ffmpeg);
				tb.Add("file 'TEMP\\tmp_" + name + "_" + time + "_f.mp4'");
				tb.Add("file 'TEMP\\tmp_" + name + "_" + time + "_r.mp4'");
				File.WriteAllLines(loc, tb);
				loc = "TEMP\\tmp_" + name + "_" + time + ".txt";
				return (loc);
			}
		}

		private static void RReverse(int size, string str1, string str2, FFmpeg ffmpeg)
		{
			ffmpeg.Run(str1, str2, FinalVariables.ReverseCommand(str1, str2, 0.8f, (int)(size / 2), 0));
		}

		private static void RCut(string input, int time, int size, string str1, FFmpeg ffmpeg)
		{
			ffmpeg.Run(input, str1, FinalVariables.CutCommand(input, str1, time, size, 0.25f));
		}

	}
}
