﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MontageConsole.Utils;
using MontageConsole.EditorUtils;
using System.IO;

namespace MontageConsole
{
    class Program
    {
        static void Main(string[] args)
        {
			FfmpegHandler.CheckFfmpegDirectory();
			FfmpegHandler.CheckFfmpegFiles();

			if (args.Length == 3 && args[0] == "-c")
			{
				if (File.Exists(args[1]))
					ConvertFile(args[1], args[2]);
				else
					PrintUsage();
			}
			else if (args.Length == 2 && args[0] == "-re")
			{
				if (File.Exists(args[1]))
					RandomEdit(args[1]);
				else
					PrintUsage();
			}
			else if (args[0] == "-re")
			{
				string[] ar = ArrayUtils.GetArrayFromPoint<string>(args, 1);
				if (FileManager.FilesExists(ar))
					RandomEdit(ar);
				else
					PrintUsage();
			}
			else
				PrintUsage();
			Console.Read();
		}

		static void RandomEdit(string file)
		{
			string input = Converters.GetBestFile(file);
			RandomEditor.RandomEdit(input);
		}

		static void RandomEdit(string[] args)
		{
			RandomEditor.RandomEdit(args);
		}

		static void ConvertFile(string file, string format)
		{
			if (format == ".flv")
				Converters.ConvertToFLV(file);
			else if (format == ".avi")
				Converters.ConvertToAVI(file);
			else
				Converters.ConvertToMP4(file);
		}

		static void PrintUsage()
		{
			Console.WriteLine("Usage :");
			Console.WriteLine("        -c [file] [new file type]");
			Console.WriteLine("        Convert File");
			Console.WriteLine("");
			Console.WriteLine("        -re [file] [file] ...");
			Console.WriteLine("        Create Random Edit with more one or more inputs");
		}
	}
}
