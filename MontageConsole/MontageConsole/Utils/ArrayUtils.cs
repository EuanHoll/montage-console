﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MontageConsole.Utils
{
	class ArrayUtils
	{
		public static T[] GetArrayFromPoint<T>(T[] array, int i)
		{
			List<T> lst = new List<T>();
			while (i < array.Length)
			{
				lst.Add(array[i]);
				i++;
			}
			return (lst.ToArray<T>());
		}
	}
}
