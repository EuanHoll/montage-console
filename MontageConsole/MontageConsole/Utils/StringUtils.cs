﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MontageConsole.Utils
{
    class StringUtils
    {
        public static string GetStringMinusExtension(string input)
        {
            int i;
			
            i = -1;
            for (int x = 0; x < input.Length; x++)
                if (input[x] == '.')
                    i = x;
			return (input.Remove(i, input.Length - i));
        }

		public static string GetStringExtension(string input)
		{
			int i;

			i = -1;
			for (int x = 0; x < input.Length; x++)
				if (input[x] == '.')
					i = x;
			return (input.Substring(i, input.Length - i));
		}

		public static string GetFileDirectory(string input)
		{
			int i;

			i = -1;
			for (int x = 0; x < input.Length; x++)
				if (input[x] == '\\')
					i = x;
			return (input.Substring(0, i));
		}

		public static string GetFileName(string input)
		{
			return (GetStringMinusExtension(input.Split('\\')[input.Split('\\').Length - 1]));
		}
	}
}
