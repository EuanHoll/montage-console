﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Javi.FFmpeg;

namespace MontageConsole.Utils
{
    class Converters
    {
        public static void ConvertToAVI(string input)
        {
			Console.WriteLine("Begining Conversion");
			using (var ffmpeg = new FFmpeg(@FinalVariables.ffmpegLoc))
			{
				string output;

				output = StringUtils.GetStringMinusExtension(input) + ".avi";
				ffmpeg.Run(input, output, FinalVariables.ConvertCommand(input, output));
			}
			Console.WriteLine("Finished Conversion");
		}

		public static void ConvertToMP4(string input)
		{
			Console.WriteLine("Begining Conversion");
			using (var ffmpeg = new FFmpeg(@FinalVariables.ffmpegLoc))
			{
				string output;

				output = StringUtils.GetStringMinusExtension(input) + ".mp4";
				ffmpeg.Run(input, output, FinalVariables.ConvertCommand(input, output));
			}
			Console.WriteLine("Finished Conversion");
		}

		public static void ConvertToFLV(string input)
		{
			Console.WriteLine("Begining Conversion");
			using (var ffmpeg = new FFmpeg(@FinalVariables.ffmpegLoc))
			{
				string output;

				output = StringUtils.GetStringMinusExtension(input) + ".flv";
				ffmpeg.Run(input, output, FinalVariables.ConvertCommand(input, output));
			}
			Console.WriteLine("Finished Conversion");
		}

		public static String GetBestFile(string input)
		{
			if (StringUtils.GetStringExtension(input) != ".mp4")
			{
				ConvertToMP4(input);
				return (StringUtils.GetStringMinusExtension(input) + ".mp4");
			}
			else
				return (input);
		}
	}
}
