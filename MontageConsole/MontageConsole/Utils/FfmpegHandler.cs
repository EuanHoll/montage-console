﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MontageConsole.Utils
{
	class FfmpegHandler
	{
		public static void CheckFfmpegDirectory()
		{
			Directory.CreateDirectory(FinalVariables.ffmpegDir);
			Directory.CreateDirectory(FinalVariables.outDir);
		}

		public static void CheckFfmpegFiles()
		{
			if (!File.Exists(FinalVariables.ffmpegLoc))
				File.WriteAllBytes(FinalVariables.ffmpegLoc, GlobalRes.ffmpeg);
		}
	}
}
