﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MontageConsole.Utils
{
	class FinalVariables
	{
		//Directory Locations
		public static String ffmpegDir = Environment.CurrentDirectory + "\\ManagedFiles";
		public static String tmpDir = Environment.CurrentDirectory + "\\TEMP";
		public static String outDir = Environment.CurrentDirectory + "\\Output";

		//File Locations
		public static String ffmpegLoc = ffmpegDir + "\\ffmpeg.exe";
		public static String fileOrdUniLoc = tmpDir + "\\fileorder.txt";
		public static String fileOrdLcLoc = "TEMP\\fileorder.txt";

		//Misc
		public static String ConvertCommand(string input, string output)
		{
			return (string.Format($"-i \"{input}\" -c:v libx264 -crf 19 -strict experimental \"{output}\""));
		}

		public static String CutCommand(string input, string output, int start, int size, float speed)
		{
			return (string.Format($"-i \"{input}\" -ss \"{start}\" -t \"{size}\" -vf \"setpts = \"{speed}\" * PTS\" \"{output}\""));
		}

		public static String MergeCommand(string input, string output)
		{
			return (string.Format($"-f concat -safe 0 -i \"{input}\" -c copy \"{output}\""));
		}

		public static String ReverseCommand(string input, string output, float speed, int size, int start)
		{
			return (string.Format($"-i \"{input}\" -ss \"{start}\" -t \"{size}\" -vf \"setpts = \"{speed}\" * PTS,reverse\" \"{output}\""));
		}

	}
}
