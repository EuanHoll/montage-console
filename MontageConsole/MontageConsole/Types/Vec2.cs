﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MontageConsole.Types
{
	class Vec2
	{
		private int x;
		private int y;

		public int X { get => x; set => x = value; }
		public int Y { get => y; set => y = value; }

		public Vec2(int x, int y)
		{
			this.X = x;
			this.Y = y;
		}

		public Vec2()
		{
			this.X = 0;
			this.Y = 0;
		}
	}
}
