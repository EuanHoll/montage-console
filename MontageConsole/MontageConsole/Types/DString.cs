﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MontageConsole.Types
{
	class DString
	{
		private string x;
		private string y;

		public string X { get => x; set => x = value; }
		public string Y { get => y; set => y = value; }

		public DString(string x, string y)
		{
			this.X = x;
			this.Y = y;
		}

		public DString()
		{
			this.X = "";
			this.Y = "";
		}
	}
}
